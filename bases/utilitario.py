
from django.http import JsonResponse


class FormularioInvalido:
    def form_invalid(self,form ):
        response = super().form_invalid(form)
        if self.request.headers.get('x-requested-with') == 'XMLHttpRequest':
            return JsonResponse(form.errors, status=400)
        else:
            return response