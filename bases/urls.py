from django.urls import path
from django.contrib.auth import views as autenticacion_vista
from .views import Inicio, Inicio_No_autorizado
urlpatterns = [
    path('', Inicio.as_view(), name='inicio'),
    path('login/', autenticacion_vista.LoginView.as_view(
        template_name='bases/login.html'), name='login'),
    path('salir/', autenticacion_vista.LogoutView.as_view(
        template_name='bases/login.html'), name='salir'),
    path('401/', Inicio_No_autorizado.as_view(), name="401"),
]
