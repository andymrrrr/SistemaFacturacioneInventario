from django.shortcuts import render
from django.views import generic
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib.auth.models import AnonymousUser

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin as Permisos


class No_autorizado(LoginRequiredMixin, Permisos):
    login_url = 'bases:login'
    raise_exception = False
    redirect_field_name = "redirecto_to"

    def handle_no_permission(self):
        if not self.request.user == AnonymousUser():
            self.login_url = 'bases:401'
        return HttpResponseRedirect(reverse_lazy(self.login_url))


class Inicio (LoginRequiredMixin, generic.TemplateView):
    template_name = 'bases/inicio.html'
    login_url = 'bases:login'


class Inicio_No_autorizado(LoginRequiredMixin, generic.TemplateView):
    login_url = 'bases:login'
    template_name = "bases/401.html"
