from django.db import models
from django.contrib.auth.models import User
from django_userforeignkey.models.fields import UserForeignKey

class ClaseModelo(models.Model):
    estado = models.BooleanField(default=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_creador = models.ForeignKey(User, on_delete=models.CASCADE)
    usuario_modificador = models.IntegerField(blank=True, null=True)

    class Meta:
        #ppara que django no lo tome en cuenta para la migracion
        abstract = True


class ClaseModelo2(models.Model):
    estado = models.BooleanField(default=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_creador = UserForeignKey(auto_user_add=True,related_name='+')
    usuario_modificador = UserForeignKey(auto_user=True,related_name='+')

    class Meta:
        abstract = True
