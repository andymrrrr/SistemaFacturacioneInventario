from django.urls import path
from .views import Producto_Listado, Producto_Detalle, Cliente_Listado, \
                   Genera_Numero_factura_compra, Genera_Codigo_Producto
urlpatterns = [
    #Api Producto
    path('v1/productos/',Producto_Listado.as_view(),name='producto_listado'),
    path('v1/productos/<str:codigo>',Producto_Detalle.as_view(),name='producto_detalle'),
    path('v1/productos/generacodigo/',Genera_Codigo_Producto.as_view(),name='genera_codigo_producto'),

    #Api Cliente
    path('v1/clientes/',Cliente_Listado.as_view(),name='cliente_list'),

    #Api Compra
    path('v1/compra/generafactura/',Genera_Numero_factura_compra.as_view(),name='genera_numero_factura'),



]
