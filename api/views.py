from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response

from django.shortcuts import get_object_or_404



from .serialize import Producto_Serializado,Cliente_Serializado
from inventario.models import Producto
from facturacion.models import Cliente, Factura_Encabezado
from compra.models import Compras_Detalle, Compras_Encabezado

from django.db.models import Q

class Producto_Listado(APIView):
    def get(self,request):
        producto = Producto.objects.all()
        data = Producto_Serializado(producto,many=True).data
        return Response(data)


class Producto_Detalle(APIView):
    def get(self,request, codigo):
        producto = get_object_or_404(Producto,Q(codigo=codigo)|Q(codigo_barra=codigo))
        data = Producto_Serializado(producto).data
        return Response(data)


class Cliente_Listado(APIView):
    def get(self,request):
        cliente = Cliente.objects.all()
        data = Cliente_Serializado(cliente,many=True).data
        return Response(data)


class Genera_Numero_factura_compra(APIView):
    def get(self,request):
        cantidad_registros = Compras_Encabezado.objects.count()
        numero_factura = f'C{cantidad_registros + 1:05}'
        return Response(numero_factura)
    

class Genera_Codigo_Producto(APIView):
    def get(self, request):
        cantidad_registro = Producto.objects.count()
        numero_codigo = f"P{cantidad_registro + 1:05}"
        return Response(numero_codigo)