from rest_framework import serializers
from inventario.models import Producto
from facturacion.models import Cliente
from compra.models import Compras_Encabezado

class Producto_Serializado(serializers.ModelSerializer):

    class Meta:
        model=Producto
        fields='__all__'


class Cliente_Serializado(serializers.ModelSerializer):

    class Meta:
        model=Cliente
        fields='__all__'

class Compra_Serializada(serializers.ModelSerializer):

    class Meta:
        model = Compras_Encabezado
        fields= '__all__'