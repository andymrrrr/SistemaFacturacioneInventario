
import os
from pathlib import Path
import dj_database_url

# BASE_DIR = Path(__file__).resolve().parent.parent
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Produccion
# """
SECRET_KEY = os.environ.get("SECRET_KEY")
DEBUG = os.environ.get("DEBUG", "False").lower() == "true"
ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS").split(" ")
# """

# Desarrollo
'''
SECRET_KEY = 'django-insecure-0^+)z59!y$brple2pr4v@8uag9%r@ems_^jkekv5)m&mjan7qn'
DEBUG = True
ALLOWED_HOSTS = []
'''
# Application definition
# Registro mis app o capas que voy a utilizar en el proyecto.
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # App nueva registradas
    'django_userforeignkey',
    'rest_framework',
    'debug_toolbar',
    'bases',
    'inventario',
    'compra',
    'facturacion',
    'api'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # Nuevos
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django_userforeignkey.middleware.UserForeignKeyMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware'
]

ROOT_URLCONF = 'app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # definir ruta de la base de la plantilla Desarrollo
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        # Producioon
        # 'DIRS': [os.path.join(BASE_DIR, 'templates/bases'),],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'app.wsgi.application'


# Conexion
# Desarrollo
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'db_facturacion',
        'HOST': "localhost",
        'USER': 'postgres',
        'PASSWORD': '965410',
        'PORT': 5432
    }
}

# producccion
# """
database_url = os.environ.get("DATABASE_URL")
DATABASES['default'] = dj_database_url.parse(database_url)
# """
# "postgres://andymrr:4r31b5wC99pZBz1mYwHp9XbCYkV21wRJ@dpg-cjbop5rbq8nc73eontkg-a.oregon-postgres.render.com/facturacion_pzf1"

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LANGUAGE_CODE = 'es-es'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True

DEBUG = True


# Desarrollo
"""
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)
STATIC_URL = 'static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
"""
# produuccion
# """
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]
STATIC_ROOT = 'static'
# """


STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/login/'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
