
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("",
         include(('bases.urls', 'bases'),
                 namespace='bases')),
    path("inventario/",
         include(('inventario.urls', 'inventario'),
                 namespace='inventario')),
    path("compra/",
         include(('compra.urls', 'compra'),
                 namespace='compra')),
    path("facturacion/",
         include(('facturacion.urls', 'facturacion'),
                 namespace='facturacion')),
    path("api/", include(('api.urls', 'api'),
                         namespace='api')),
    path('admin/', admin.site.urls),
]
