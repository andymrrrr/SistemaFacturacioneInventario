from django import forms

from .models import Categoria,SubCategoria,Marca,UnidadMedida,Producto


class CategoriaFormulario(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ["descripcion","estado"]
        labels ={
            "descripcion":"Categoria",
            "estado":"Estado"
            }
        widgets = {
            "descripcion": forms.TextInput()
        }
    def __init__(self, *args, **kwargs):
        super(CategoriaFormulario, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
          self.fields[field].widget.attrs.update({
              'class':'form-control'
          })


class SubCategoriaFormulario(forms.ModelForm):
    categoria = forms.ModelChoiceField(
        queryset= Categoria.objects.filter(estado=True).order_by('descripcion')
    )
    class Meta:
        model = SubCategoria
        fields = ["categoria","descripcion","estado"]
        labels ={
                "categoria": "Categoria",
                "descripcion":"Sub Categoria",
                "estado":"Estado"}
        widgets = {
            "descripcion": forms.TextInput()
        }
    def __init__(self, *args, **kwargs):
        super(SubCategoriaFormulario, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
          self.fields[field].widget.attrs.update({
              'class':'form-control'
          })
        self.fields['categoria'].empty_label= "seleccione"


class MarcaFormulario(forms.ModelForm):
    class Meta:
        model=Marca
        fields = ['descripcion','estado']
        labels= {
            "descripcion": "Nombre",
            "estado":"Estado"}
        widget={'descripcion': forms.TextInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })



class UnidadMedidaFormulario(forms.ModelForm):
    class Meta:
        model=UnidadMedida
        fields = ['descripcion','estado']
        labels= {'descripcion': "Descripción de la Marca",
                "estado":"Estado"}
        widget={'descripcion': forms.TextInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class Producto_Formulario(forms.ModelForm):
    class Meta:
        model= Producto
        fields=['codigo','codigo_barra','descripcion','estado', \
                'precio','existencia','ultima_compra',
                'marca','subcategoria','unidad_medida']
        exclude = ['usuario_modificador','fecha_modificacion','usuario_creador','usuario_modificador']
        widget={'descripcion': forms.TextInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
        self.fields['ultima_compra'].widget.attrs['readonly'] = True
        self.fields['existencia'].widget.attrs['readonly'] = True
        self.fields['codigo'].widget.attrs['readonly'] = True