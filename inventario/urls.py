from django.urls import path
from .views import Categoria_Vista, Categoria_Agregar, Categoria_Editar,Categoria_Inactivar,\
                   SubCategoria_Vista, SubCategoria_Agregar , SubCategoria_Editar,SubCategoria_Inactivar, \
                   Marca_Vista, Marca_Agregar,Marca_Editar, Marca_Inactivar, \
                   Unidad_Medida_Vista,Unidad_Medidad_Agregar, Unidad_Medida_Editar, Unidad_Medida_Inactivar, \
                   Producto_Vista, Producto_Agregar,Producto_Editar,Producto_Inactivar

urlpatterns = [
    #Categorias
    path('categorias/', Categoria_Vista.as_view(), name='categorias'),
    path('categorias/agregar/', Categoria_Agregar.as_view(), name='categoria_agregar'),
    path('categorias/editar/<int:pk>/', Categoria_Editar.as_view(), name='categoria_editar'),
    path('categorias/estado/<int:id>', Categoria_Inactivar, name='categoria_inactivar'),

    #SubCategorias
    path('subcategorias/', SubCategoria_Vista.as_view(), name='subcategorias'),
    path('subcategorias/agregar/', SubCategoria_Agregar.as_view(), name='subcategoria_agregar'),
    path('subcategorias/editar/<int:pk>/', SubCategoria_Editar.as_view(), name='subcategoria_editar'),
    path('subcategorias/estado/<int:id>', SubCategoria_Inactivar, name='subcategoria_inactivar'),

    #Marcas
    path('marcas/', Marca_Vista.as_view(), name='marcas'),
    path('marcas/agregar/', Marca_Agregar.as_view(), name='marca_agregar'),
    path('marcas/editar/<int:pk>', Marca_Editar.as_view(), name='marca_editar'),
    path('marcas/estado/<int:id>', Marca_Inactivar, name='marca_inactivar'),

    #Unidad de Medida
    path('unidamedidas/', Unidad_Medida_Vista.as_view(), name='unidadmedidas'),
    path('unidamedidas/agregar/', Unidad_Medidad_Agregar.as_view(), name='unidadmedida_agregar'),
    path('unidamedidas/editar/<int:pk>', Unidad_Medida_Editar.as_view(), name='unidadmedida_editar'),
    path('unidamedidas/estado/<int:id>',Unidad_Medida_Inactivar, name='unidadmedida_inactivar'),

    #productos
    path('productos/', Producto_Vista.as_view(), name='productos'),
    path('productos/agregar', Producto_Agregar.as_view(), name='productos_agregar'),
    path('productos/editar/<int:pk>', Producto_Editar.as_view(), name='productos_editar'),
    path('productos/estado/<int:id>', Producto_Inactivar, name='productos_inactivar'),

]
