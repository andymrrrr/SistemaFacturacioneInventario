from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required, permission_required
from bases.views import No_autorizado
from .models import Categoria, SubCategoria, Marca, UnidadMedida, Producto
from .forms import CategoriaFormulario, SubCategoriaFormulario, MarcaFormulario,UnidadMedidaFormulario,Producto_Formulario

#Categoria
class Categoria_Vista(No_autorizado,generic.ListView):
    model = Categoria
    template_name="inventario/categorias.html"
    permission_required = "inventario.view_categoria"
    context_object_name = 'categorias'


class Categoria_Agregar(SuccessMessageMixin,No_autorizado,generic.CreateView):
    model = Categoria
    template_name="inventario/categoria_formulario.html"
    permission_required ="inventario.add_categoria"
    context_object_name = "categoria"
    form_class = CategoriaFormulario
    success_url= reverse_lazy('inventario:categorias')
    success_message = "Categoria Agregada Correctamente"

    def form_valid(self, form):
       form.instance.usuario_creador = self.request.user
       return super().form_valid(form)

class Categoria_Editar(SuccessMessageMixin,No_autorizado,generic.UpdateView):
    model = Categoria
    template_name="inventario/categoria_formulario.html"
    context_object_name = "categoria"
    permission_required ="inventario.change_categoria"
    form_class = CategoriaFormulario
    success_url= reverse_lazy('inventario:categorias')
    success_message = "Categoria Editada Correctamente"

    def form_valid(self, form):
       form.instance.usuario_modificador = self.request.user.id
       return super().form_valid(form)



def Categoria_Inactivar(request,id):
    categoria = Categoria.objects.filter(pk=id).first()
    if  request.method=="POST":
        if categoria:
            categoria.estado = not categoria.estado
            categoria.save()
            return HttpResponse("OK")
        return HttpResponse("FAIL")
    return HttpResponse("FAIL")

#Sub Categoria
class SubCategoria_Vista(No_autorizado,generic.ListView):
    model = SubCategoria
    template_name="inventario/subcategorias.html"
    permission_required = "inventario.view_subcategoria"
    context_object_name = 'subcategorias'


class SubCategoria_Agregar(SuccessMessageMixin,No_autorizado,generic.CreateView):
    model = SubCategoria
    template_name="inventario/subcategoria_formulario.html"
    context_object_name = "subcategoria"
    permission_required = "inventario.add_subcategoria"
    form_class = SubCategoriaFormulario
    success_url= reverse_lazy('inventario:subcategorias')
    success_message = "SubCategoria Agregada Correctamente"

    def form_valid(self, form):
       form.instance.usuario_creador = self.request.user
       return super().form_valid(form)
    

class SubCategoria_Editar(SuccessMessageMixin,No_autorizado,generic.UpdateView):
    model = SubCategoria
    template_name="inventario/subcategoria_formulario.html"
    context_object_name = "subcategoria"
    permission_required = "inventario.change_subcategoria"
    form_class = SubCategoriaFormulario
    success_url= reverse_lazy('inventario:subcategorias')
    success_message = "SubCategoria Edita Correctamente"

    def form_valid(self, form):
       form.instance.usuario_modificador = self.request.user.id
       return super().form_valid(form)


def SubCategoria_Inactivar(request,id):
    subcategoria = SubCategoria.objects.filter(pk=id).first()
    if  request.method=="POST":
        if subcategoria:
            subcategoria.estado = not subcategoria.estado
            subcategoria.save()
            return HttpResponse("OK")
        return HttpResponse("FAIL")
    return HttpResponse("FAIL")

#Marcas
class Marca_Vista(No_autorizado,generic.ListView):
    model = Marca
    template_name="inventario/marcas.html"
    context_object_name = 'marcas'
    permission_required = "inventario.view_marca"


class Marca_Agregar(SuccessMessageMixin,No_autorizado,generic.CreateView):
    model = Marca
    template_name="inventario/marca_formulario.html"
    context_object_name = "marca"
    permission_required = "inventario.add_marca"
    form_class = MarcaFormulario
    success_url= reverse_lazy('inventario:marcas')
    success_message = "Marca Agregada Correctamente"

    def form_valid(self, form):
       form.instance.usuario_creador = self.request.user
       return super().form_valid(form)
    

class Marca_Editar(SuccessMessageMixin,No_autorizado,generic.UpdateView):
    model = Marca
    template_name="inventario/marca_formulario.html"
    context_object_name = "marca"
    permission_required = "inventario.change_marca"
    form_class = MarcaFormulario
    success_url= reverse_lazy('inventario:marcas')
    success_message = "Marca Editada Correctamente"

    def form_valid(self, form):
       form.instance.usuario_modificador = self.request.user.id
       return super().form_valid(form)

@login_required(login_url="bases:login")
@permission_required("inventario.delete_marca",login_url="bases:401.html")
def Marca_Inactivar(request,id):
    marca = Marca.objects.filter(pk=id).first()
    if  request.method=="POST":
        if marca:
            marca.estado = not marca.estado
            marca.save()
            return HttpResponse("OK")
        return HttpResponse("FAIL")
    return HttpResponse("FAIL")


#Unidad de Medida
class Unidad_Medida_Vista(No_autorizado,generic.ListView):
    model = UnidadMedida
    template_name="inventario/unidad_medidas.html"
    context_object_name = 'unidadmedidas'
    permission_required = "inventario.view_unidadmedida"


class Unidad_Medidad_Agregar(SuccessMessageMixin,No_autorizado,generic.CreateView):
    model = UnidadMedida
    template_name="inventario/unidad_medida_formulario.html"
    context_object_name = "unidadmedida"
    permission_required = "inventario.add_unidadmedida"
    form_class = UnidadMedidaFormulario
    success_url= reverse_lazy('inventario:unidadmedidas')
    success_message = "Unidad de Medida Agregada Correctamente"

    def form_valid(self, form):
       form.instance.usuario_creador = self.request.user
       return super().form_valid(form)
    

class Unidad_Medida_Editar(SuccessMessageMixin,No_autorizado,generic.UpdateView):
    model = UnidadMedida
    template_name="inventario/unidad_medida_formulario.html"
    context_object_name = "unidadmedida"
    permission_required = "inventario.change_unidadmedida"
    form_class = UnidadMedidaFormulario
    success_url= reverse_lazy('inventario:unidadmedidas')
    success_message = "Unidad de Medida Editada Correctamente"

    def form_valid(self, form):
       form.instance.usuario_modificador = self.request.user.id
       return super().form_valid(form)



@login_required(login_url="bases:login")
@permission_required("inventario.unidadmedida",login_url="bases:401.html")
def Unidad_Medida_Inactivar(request,id):
    unidad_medida = UnidadMedida.objects.filter(pk=id).first()
    if  request.method=="POST":
        if unidad_medida:
            unidad_medida.estado = not unidad_medida.estado
            unidad_medida.save()
            return HttpResponse("OK")
        return HttpResponse("FAIL")
    return HttpResponse("FAIL")

    
#Producto
class Producto_Vista(No_autorizado,generic.ListView):
    model = Producto
    template_name="inventario/productos.html"
    context_object_name = 'productos'
    permission_required = "inventario.view_producto"
    



class Producto_Agregar(SuccessMessageMixin,No_autorizado,generic.CreateView):
    model = Producto
    template_name="inventario/producto_formulario.html"
    context_object_name = "producto"
    permission_required = "inventario.add_producto"
    form_class = Producto_Formulario
    success_url= reverse_lazy('inventario:productos')
    success_message = "Producto Agregado Correctamente"

    def form_valid(self, form):
       form.instance.usuario_creador = self.request.user
       return super().form_valid(form)
    
   
    def get_context_data(self, **kwargs):
        context = super(Producto_Agregar, self).get_context_data(**kwargs)
        context["categorias"] = Categoria.objects.all()
        context["subcategorias"] = SubCategoria.objects.all()
        return context


class Producto_Editar(No_autorizado,generic.UpdateView):
    model = Producto
    template_name="inventario/producto_formulario.html"
    context_object_name = "producto"
    permission_required = "inventario.change_producto"
    form_class = Producto_Formulario
    success_url= reverse_lazy('inventario:productos')
    success_message = "Producto Editado Correctamente"

    def form_valid(self, form):
       form.instance.usuario_modificador = self.request.user.id
       return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
        pk = self.kwargs.get('pk')

        context = super(Producto_Editar, self).get_context_data(**kwargs)
        context["categorias"] = Categoria.objects.all()
        context["subcategorias"] = SubCategoria.objects.all()
        context["producto"] = Producto.objects.filter(pk=pk).first()

        return context


@login_required(login_url="bases:login")
@permission_required("inventario.delete_unidadmedida",login_url="bases:401.html")
def Producto_Inactivar(request,id):
    producto = Producto.objects.filter(pk=id).first()
    if  request.method=="POST":
        if producto:
            producto.estado = not producto.estado
            producto.save()
            return HttpResponse("OK")
        return HttpResponse("FAIL")
    return HttpResponse("FAIL")
    