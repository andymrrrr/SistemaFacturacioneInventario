from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.db.models import Sum

from inventario.models import Producto
from bases.models import ClaseModelo, ClaseModelo2

class Cliente(ClaseModelo):
    FIS='Fisiica'
    EMP='Empresa'
    TIPO_CLIENTE = [
        (FIS,'Fisiica'),
        (EMP,'Empresa')
    ]
    nombres = models.CharField(
        max_length=100
    )
    apellidos = models.CharField(
        max_length=100
    )
    celular = models.CharField(
        max_length=20,
        null=True,
        blank=True
    )
    tipo=models.CharField(
        max_length=10,
        choices=TIPO_CLIENTE,
        default=FIS
    )

    def __str__(self):
        return '{} {}'.format(self.apellidos,self.nombres)

    def save(self, *args, **kwargs):
        self.nombres = self.nombres.upper()
        self.apellidos = self.apellidos.upper()
        super(Cliente, self).save( *args, **kwargs)

    class Meta:
        verbose_name_plural = "Clientes"


class Factura_Encabezado(ClaseModelo2):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)
    sub_total=models.FloatField(default=0)
    descuento=models.FloatField(default=0)
    total=models.FloatField(default=0)

    def __str__(self):
        return '{}'.format(self.id)

    def save(self):
        self.total = self.sub_total - self.descuento
        super(Factura_Encabezado,self).save()

    class Meta:
        verbose_name_plural = "Encabezado Facturas"
        verbose_name="Encabezado Factura"
    

class Factura_Detalle(ClaseModelo2):
    factura = models.ForeignKey(Factura_Encabezado,on_delete=models.CASCADE)
    producto=models.ForeignKey(Producto,on_delete=models.CASCADE)
    cantidad=models.BigIntegerField(default=0)
    precio=models.FloatField(default=0)
    sub_total=models.FloatField(default=0)
    descuento=models.FloatField(default=0)
    total=models.FloatField(default=0)

    def __str__(self):
        return '{}'.format(self.producto)

    def save(self):
        self.sub_total = float(float(int(self.cantidad)) * float(self.precio))
        self.total = self.sub_total - float(self.descuento)
        super(Factura_Detalle, self).save()
    
    class Meta:
        verbose_name_plural = "Detalles Facturas"
        verbose_name="Detalle Factura"
        permissions = [
            ('supervisor_caja','Permisos de Supervisor de Caja Detalle')
        ]

      

@receiver(post_save, sender=Factura_Detalle)
def detalle_fac_guardar(sender,instance,**kwargs):
    factura_id = instance.factura.id
    producto_id = instance.producto.id

    factura_encabezado = Factura_Encabezado.objects.get(pk=factura_id)
    if factura_encabezado:
        sub_total = Factura_Detalle.objects.filter(factura=factura_id).aggregate(sub_total=Sum('sub_total')).get('sub_total',0.00)
        descuento = Factura_Detalle.objects.filter(factura=factura_id).aggregate(descuento=Sum('descuento')).get('descuento',0.00)  
        factura_encabezado.sub_total = sub_total
        factura_encabezado.descuento = descuento
        factura_encabezado.save()

    producto=Producto.objects.filter(pk=producto_id).first()
    if producto:
        cantidad = int(producto.existencia) - int(instance.cantidad)
        producto.existencia = cantidad
        producto.save()



