from django.shortcuts import render
from django.utils.dateparse import parse_date
from datetime import timedelta

from .models import Factura_Detalle,Factura_Encabezado

def imprimir_factura_recibo(request,id):
    template_name="facturacion/factura_imprimir_una.html"

    factura_encabezado = Factura_Encabezado.objects.get(id=id)
    factura_detalle = Factura_Detalle.objects.filter(factura=id)

    context={
        'request':request,
        'factura_encabezado':factura_encabezado,
        'factura_detalle':factura_detalle
    }

    return render(request,template_name,context)


def imprimir_todas_factura(request,fechainicio,fechafinal):
    template_name="facturacion/facturas_imprimir_todas.html"

    fechainicio=parse_date(fechainicio)
    fechafinal=parse_date(fechafinal)
    fechafinal=fechafinal + timedelta(days=1)

    factura_encabezado = Factura_Encabezado.objects.filter(fecha__gte=fechainicio,fecha__lt=fechafinal)
    fechafinal=fechafinal - timedelta(days=1)
    
    context = {
        'request':request,
        'fechainicio':fechainicio,
        'fechafinal':fechafinal,
        'factura_encabezado':factura_encabezado
    }

    return render(request,template_name,context)
