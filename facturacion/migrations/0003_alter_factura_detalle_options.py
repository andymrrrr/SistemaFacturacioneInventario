# Generated by Django 4.2.3 on 2023-07-30 23:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('facturacion', '0002_factura_encabezado_factura_detalle'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='factura_detalle',
            options={'permissions': [('supervisor_caja', 'Permisos de Supervisor de Caja Detalle')], 'verbose_name': 'Detalle Factura', 'verbose_name_plural': 'Detalles Facturas'},
        ),
    ]
