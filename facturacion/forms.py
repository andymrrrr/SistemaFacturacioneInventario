from django import forms

from .models import Cliente
class Cliente_Formulario(forms.ModelForm):
    class Meta:
        model=Cliente
        fields=['nombres','apellidos','tipo','celular','estado']
        labels={
            "nombres": "Nombres",
            "apellidos": "Apellidos",
            "tipo": "Tipo Cliente",
            "celular": "Celular",
            "estado":"Estado"
        }
        exclude = ['usuario_modificador','fecha_modificacion','usuario_creador','fecha_creacion']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })