from django.urls import path
from .views import Clientes_Vista, Cliente_Agregar, Cliente_Editar, cliente_Inactivar ,\
        Facturas_vista, facturas, Producto_vista, borrar_detalle_factura

from .reporte import imprimir_factura_recibo, imprimir_todas_factura

urlpatterns = [
    #cliente
    path('cliente/',Clientes_Vista.as_view(),name="clientes"),
    path('cliente/agregar',Cliente_Agregar.as_view(),name="cliente_agregar"),
    path('cliente/editar/<int:pk>',Cliente_Editar.as_view(),name="cliente_editar"),
    path('cliente/estado/<int:id>',cliente_Inactivar,name="cliente_inactivar"),
    #factura
    path('facturas/',Facturas_vista.as_view(),name="facturas_lista"),
    path('facturas/agregar',facturas,name="facturas_agregar"),
    path('facturas/editar/<int:id>',facturas, name="factura_editar"),
    path('facturas/borrar_detalle/<int:id>',borrar_detalle_factura, name="factura_borrar_detalle"),
    path('facturas/buscar-producto',Producto_vista.as_view(), name="factura_producto"),
    path('facturas/imprimir/<int:id>',imprimir_factura_recibo, name="factura_imprimir_una"),
    path('facturas/imprimir-todas/<str:fechainicio>/<str:fechafinal>',imprimir_todas_factura, name="factura_imprimir_todas"),


]
