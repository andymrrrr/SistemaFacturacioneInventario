from django.shortcuts import render,redirect
from django.views import generic
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from datetime import datetime
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import authenticate
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.contrib import messages
from bases.views import No_autorizado
from inventario.models import Producto
import inventario.views as inventario

from .models import Cliente, Factura_Encabezado, Factura_Detalle
from .forms import Cliente_Formulario
#Clientes
class Clientes_Vista(No_autorizado,generic.ListView):
    model = Cliente
    template_name="facturacion/clientes.html"
    context_object_name = 'clientes'
    permission_required="facturacion.view_cliente"


class Cliente_Agregar(SuccessMessageMixin,No_autorizado,generic.CreateView):
    model=Cliente
    template_name="facturacion/cliente_formulario.html"
    context_object_name = 'cliente'
    success_message="Registro Agregado Satisfactoriamente"
    form_class= Cliente_Formulario
    success_url= reverse_lazy("facturacion:clientes")
    permission_required="facturacion.add_cliente"

    def get(self, request, *args, **kwargs):
        print("sobre escribir get")
        
        try:
            t = request.GET["t"]
        except:
            t = None

        print(t)
        
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form, 't':t})
    
    def form_valid(self, form):
       form.instance.usuario_creador = self.request.user
       return super().form_valid(form)


class Cliente_Editar(SuccessMessageMixin,No_autorizado, generic.UpdateView):
    model = Cliente
    template_name="facturacion/cliente_formulario.html"
    form_class=Cliente_Formulario
    success_url= reverse_lazy("facturacion:clientes")
    context_object_name = 'cliente'
    permission_required="facturacion.change_cliente"
    success_message = "Cliente Modificado Correctamente"
 
    def form_valid(self, form):
       form.instance.usuario_modificador = self.request.user.id
       return super().form_valid(form)


@login_required(login_url="/login/")
@permission_required("facturacion.change_cliente",login_url="/login/")
def cliente_Inactivar(request,id):
    cliente = Cliente.objects.filter(pk=id).first()

    if request.method=="POST":
        if cliente:
            cliente.estado = not cliente.estado
            cliente.save()
            return HttpResponse("OK")
        return HttpResponse("FAIL")
    
    return HttpResponse("FAIL")


class Facturas_vista(No_autorizado, generic.ListView):
    model = Factura_Encabezado
    template_name = "facturacion/facturas_listado.html"
    context_object_name = "facturas"
    permission_required="facturacion.view_factura_encabezado"


@login_required(login_url='bases:login')
@permission_required('facturacion.change_factura_encabezado', login_url='bases:401.html')
def facturas(request,id=None):
    template_name='facturacion/facturas.html'

    factura_detalle = {}
    factura_encabezado ={}
    
    clientes = Cliente.objects.filter(estado=True)
    if request.method == 'GET':
        encabezado = Factura_Encabezado.objects.filter(pk=id).first()
        if not encabezado:
            factura_encabezado ={
                "id":0,
                "fecha":datetime.today(),
                "cliente":0,
                'sub_total':0.00,
                'descuento':0.00,
                'total': 0.00
            }
            factura_detalle = None
        else:
            factura_encabezado ={
                "id":encabezado.id,
                "fecha":encabezado.fecha,
                "cliente":encabezado.cliente,
                'sub_total':encabezado.sub_total,
                'descuento':encabezado.descuento,
                'total': encabezado.total
            }
            factura_detalle = Factura_Detalle.objects.filter(factura=encabezado)

        contexto ={
            "factura_encabezado":factura_encabezado,
            "factura_detalle":factura_detalle,
            "clientes":clientes 
            }
        print(contexto.get("factura_encabezado"))
        return render(request,template_name,contexto)
    if request.method == 'POST':
        idcliente = request.POST.get("factura_encabezado_cliente")
        fecha  = request.POST.get("factura_encabezado_fecha")
        cliente = Cliente.objects.filter(pk=idcliente).first()
        if not id:
            factura_encabezado = Factura_Encabezado(
                cliente = cliente,
                fecha = fecha
            )
            if factura_encabezado:
                factura_encabezado.save()
                id = factura_encabezado.id
        else:
            factura_encabezado = Factura_Encabezado.objects.filter(pk=id).first()
            if factura_encabezado:
                factura_encabezado.cliente = cliente
                factura_encabezado.save()
        if not id:
            messages.error(request,'No Puedo Continuar No Pude Detectar No. de Factura')
            return redirect("facturacion:facturas_listado")
        codigo = request.POST.get("codigo")
        cantidad = request.POST.get("cantidad")
        precio = request.POST.get("precio")
        s_total = request.POST.get("sub_total_detalle")
        descuento = request.POST.get("descuento_detalle")
        total = request.POST.get("total_detalle")

        prod = Producto.objects.get(codigo=codigo)
        factura_detalle = Factura_Detalle(
            factura = factura_encabezado,
            producto = prod,
            cantidad = cantidad,
            precio = precio,
            sub_total = s_total,
            descuento = descuento,
            total = total
        )
        
        if factura_detalle:
            factura_detalle.save()
        
        return redirect("facturacion:factura_editar",id=id)

 
class Producto_vista(inventario.Producto_Vista):
    template_name ="facturacion/buscar_producto.html"


def borrar_detalle_factura(request, id):
    template_name = "facturacion/factura_borrar_datalle.html"

    factura_detalle = Factura_Detalle.objects.get(pk=id)

    if request.method=="GET":
        context={"factura_detalle":factura_detalle}

    if request.method == "POST":
        usr = request.POST.get("usuario")
        pas = request.POST.get("pass")

        user =authenticate(username=usr,password=pas)

        if not user:
            return HttpResponse("Usuario o Clave Incorrecta")
        
        if not user.is_active:
            return HttpResponse("Usuario Inactivo")

        if user.is_superuser or user.has_perm("facturacion.supervisor_caja"):
            factura_detalle.id = None
            factura_detalle.cantidad = (-1 * factura_detalle.cantidad)
            factura_detalle.sub_total = (-1 * factura_detalle.sub_total)
            factura_detalle.descuento = (-1 * factura_detalle.descuento)
            factura_detalle.total = (-1 * factura_detalle.total)
            factura_detalle.save()

            return HttpResponse("ok")

        return HttpResponse("Usuario no autorizado")
    
    return render(request,template_name,context)
