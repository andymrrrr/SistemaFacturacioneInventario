from django import forms

from .models import Proveedor, Compras_Encabezado

class Proveedor_Formulario(forms.ModelForm):
    email = forms.EmailField(max_length=254)
    class Meta:
        model=  Proveedor
        fields = ['contacto','telefono',"email","direccion","descripcion","estado"]
        labels= {
            "contacto": "Nombre",
            "telefono": "Telefono",
            "email": "Email",
            "direccion": "Direccion",
            "descripcion": "Nota",
            "estado":"Estado"
        }
        exclude = ['usuario_modificador','fecha_modificacion','usuario_creador','fecha_creacion']
        widget={'descripcion': forms.TextInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

    def clean(self):
        try:
            proveedor = Proveedor.objects.get(
                contacto=self.cleaned_data["contacto"]
            )

            if not self.instance.pk:
                print("Registro ya existe")
                raise forms.ValidationError("Ya existe Proveedor con este nombre.")
            elif self.instance.pk!=proveedor.pk:
                print("Cambio no permitido")
                raise forms.ValidationError("Cambio No Permitido")
        except Proveedor.DoesNotExist:
            pass
        return self.cleaned_data

class Compras_Encabezado_Formulario(forms.ModelForm):
    fecha_compra = forms.DateInput()
    fecha_factura = forms.DateInput()
    
    class Meta:
        model= Compras_Encabezado
        fields=['proveedor','fecha_compra','observacion',
            'numero_factura','fecha_factura','sub_total',
            'descuento','total']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
        self.fields['fecha_compra'].widget.attrs['readonly'] = True
        self.fields['fecha_factura'].widget.attrs['readonly'] = True
        self.fields['sub_total'].widget.attrs['readonly'] = True
        self.fields['descuento'].widget.attrs['readonly'] = True
        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['numero_factura'].widget.attrs['readonly'] = True
   