from django.forms.models import BaseModelForm
from django.shortcuts import render, redirect
from django.views import generic
from django.http import HttpResponse, JsonResponse
import datetime
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy

from django.db.models import Sum
from .models import Proveedor, Compras_Detalle, Compras_Encabezado
from .forms import Proveedor_Formulario, Compras_Encabezado_Formulario
from inventario.models import Producto
from bases.views import No_autorizado
from bases.utilitario import FormularioInvalido
import ipdb as debug

#Proveedores
class Proveedores_Vista(No_autorizado,generic.ListView):
    model = Proveedor
    template_name="compra/proveedores.html"
    context_object_name = 'proveedores'
    permission_required="compra.view_proveedor"


class Proveedor_Agregar(SuccessMessageMixin,FormularioInvalido,No_autorizado,generic.CreateView):
    model = Proveedor
    template_name="compra/proveedor_formulario.html"
    context_object_name = 'proveedor'
    permission_required="compra.add_proveedor"
    form_class = Proveedor_Formulario
    success_url= reverse_lazy('compra:proveedores')
    success_message="Provedor Agregado Correctamente"

    def form_valid(self, form):
       form.instance.usuario_creador = self.request.user
       return super().form_valid(form)
    


class Proveedor_Editar(SuccessMessageMixin,No_autorizado,FormularioInvalido,generic.UpdateView):
    model = Proveedor
    template_name="compra/proveedor_formulario.html"
    context_object_name = 'proveedor'
    permission_required="compra.change_proveedor"
    form_class = Proveedor_Formulario
    success_url= reverse_lazy('compra:proveedores')
    success_message="Provedor Editado Correctamente"

    def form_valid(self, form):
       form.instance.usuario_modificador = self.request.user.id
       return super().form_valid(form)
    

@login_required(login_url="bases:login")
@permission_required("inventario.delete_proveedor",login_url="bases:401.html")
def Proveedor_Inactivar(request, id):
    proveedor = Proveedor.objects.filter(pk =id).first()
    contexto ={}
    template_name ="compra/proveedor_inactivar.html"

    if not proveedor:
        return HttpResponse("Proveedor no existe " + str(id))
    
    if request.method =='GET':
        contexto = {'proveedor':proveedor}
    
    if request.method == 'POST':
        proveedor.estado = False
        proveedor.save()
        contexto ={'proveedor': 'Ok'}
        return HttpResponse("Proveedor Inactvo")
    
    return render(request, template_name,contexto)

#Compra
class Compra_Vista(No_autorizado,generic.ListView):
    model = Compras_Encabezado
    template_name="compra/compra_lista.html"
    context_object_name = 'compras'
    permission_required="compra.view_compras_detalle"


@login_required(login_url="bases:login")
@permission_required("compra.view_compras_detalle",login_url="bases:401.html")
def Compras(request, compra_id = None):
    template_name = "compra/compras.html"
    producto = Producto.objects.filter(estado = True)
    formulario_compras ={}
    contexto={}

    if request.method == 'GET':
        formulario_compras = Compras_Encabezado_Formulario()
        encabezado = Compras_Encabezado.objects.filter(pk=compra_id).first()

        if encabezado:
            detalle = Compras_Detalle.objects.filter(compra=encabezado)
            fecha_compra = datetime.date.isoformat(encabezado.fecha_compra)
            fecha_factura = datetime.date.isoformat(encabezado.fecha_factura)
            valores_mapeado = {
                'fecha_compra':fecha_compra,
                'proveedor': encabezado.proveedor,
                'observacion': encabezado.observacion,
                'numero_factura': encabezado.numero_factura,
                'fecha_factura': fecha_factura,
                'sub_total': encabezado.sub_total,
                'descuento': encabezado.descuento,
                'total':encabezado.total
            }
            formulario_compras = Compras_Encabezado_Formulario(valores_mapeado)
        else:
            detalle = None
        
        contexto = {
            "productos" : producto,
            "encabezado_compra": encabezado,
            "detalle_compra": detalle,
            "formulario_encabezado": formulario_compras
        }

    if request.method == 'POST':
        fecha_compra = request.POST.get("fecha_compra")
        observacion = request.POST.get("observacion")
        numero_factura = request.POST.get("numero_factura")
        fecha_factura = request.POST.get("fecha_factura")
        proveedor = request.POST.get("proveedor")
        sub_total = 0
        descuento = 0
        total = 0
        if not compra_id:
            prove=Proveedor.objects.get(pk=proveedor)

            encabezado = Compras_Encabezado(
                fecha_compra = fecha_compra,
                observacion = observacion,
                numero_factura = numero_factura,
                fecha_factura = fecha_factura,
                proveedor= prove,
                usuario_creador = request.user 
            )
            if encabezado:
                encabezado.save()
                compra_id = encabezado.id
        else:
            encabezado=Compras_Encabezado.objects.filter(pk=compra_id).first()
            if encabezado:
                encabezado.fecha_compra = fecha_compra
                encabezado.observacion = observacion
                encabezado.numero_factura=numero_factura
                encabezado.fecha_factura=fecha_factura
                encabezado.usuario_modificador=request.user.id
                encabezado.save()

        if not compra_id:
            return redirect("compra:compras")
        
        producto = request.POST.get("id_id_producto")
        cantidad = request.POST.get("id_cantidad_detalle")
        precio = request.POST.get("id_precio_detalle")
        sub_total_detalle = request.POST.get("id_sub_total_detalle")
        descuento_detalle  = request.POST.get("id_descuento_detalle")
        total_detalle  = request.POST.get("id_total_detalle")

        prod = Producto.objects.get(pk=producto)

        detalle = Compras_Detalle(
            compra = encabezado,
            producto = prod,
            cantidad = cantidad,
            precio_prv = precio,
            descuento = descuento_detalle,
            costo = 0,
            usuario_creador = request.user
        )

        if detalle:
            detalle.save()

            sub_total=Compras_Detalle.objects.filter(compra=compra_id).aggregate(Sum('sub_total'))
            descuento=Compras_Detalle.objects.filter(compra=compra_id).aggregate(Sum('descuento'))
            encabezado.sub_total = sub_total["sub_total__sum"]
            encabezado.descuento=descuento["descuento__sum"]
            encabezado.save()

        return redirect("compra:compras_editar",compra_id=compra_id)

    return render (request, template_name,contexto)




class Compra_Detalle_Eliminar(No_autorizado, generic.DeleteView):
    permission_required = "compra.delete_compras_detalle"
    model = Compras_Detalle
    template_name = "compra/compras_detalle_elimiinar.html"
    context_object_name = 'compra_detalle'
    
    def get_success_url(self):
          compra_id=self.kwargs['compra_id']
          return reverse_lazy('compra:compras_editar', kwargs={'compra_id': compra_id})