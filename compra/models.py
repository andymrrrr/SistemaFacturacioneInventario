from django.db import models
from bases.models import ClaseModelo
#Para los signals
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from django.db.models import Sum
from inventario.models import Producto

class Proveedor(ClaseModelo):
    descripcion = models.CharField(
        max_length=100,
        null=True, blank=True
        )
    direccion = models.CharField(
        max_length=250,
        null=True, blank=True
        )
    contacto = models.CharField(
        max_length=100,
        unique=True
    )
    telefono = models.CharField(
        max_length=10,
        null=True, blank=True
    )
    email = models.CharField(
        max_length=250,
        null=True, blank=True
    )

    def __str__(self):
        return '{}'.format(self.contacto)

    def save(self):
        self.contacto = self.contacto.upper()
        super(Proveedor, self).save()

    class Meta:
        verbose_name_plural = "Proveedores"



class Compras_Encabezado(ClaseModelo):
    fecha_compra=models.DateField(null=True,blank=True)
    observacion=models.TextField(blank=True,null=True)
    numero_factura=models.CharField(max_length=100)
    fecha_factura=models.DateField()
    sub_total=models.FloatField(default=0)
    descuento=models.FloatField(default=0)
    total=models.FloatField(default=0)

    proveedor=models.ForeignKey(Proveedor,on_delete=models.CASCADE)
    
    def __str__(self):
        return '{}'.format(self.observacion)

    def save(self):
        self.observacion = self.observacion.upper()
        if self.sub_total == None  or self.descuento == None:
            self.sub_total = 0
            self.descuento = 0
            
        self.total = self.sub_total - self.descuento
        super(Compras_Encabezado,self).save()

    class Meta:
        verbose_name_plural = "Encabezado Compras"
        verbose_name="Encabezado Compra"


class Compras_Detalle(ClaseModelo):
    compra=models.ForeignKey(Compras_Encabezado,on_delete=models.CASCADE)
    producto=models.ForeignKey(Producto,on_delete=models.CASCADE)
    cantidad=models.BigIntegerField(default=0)
    precio_prv=models.FloatField(default=0)
    sub_total=models.FloatField(default=0)
    descuento=models.FloatField(default=0)
    total=models.FloatField(default=0)
    costo=models.FloatField(default=0)

    def __str__(self):
        return '{}'.format(self.producto)

    def save(self):
        self.sub_total = float(float(int(self.cantidad)) * float(self.precio_prv))
        self.total = self.sub_total - float(self.descuento)
        super(Compras_Detalle, self).save()
    
    class Mega:
        verbose_name_plural = "Detalles Compras"
        verbose_name="Detalle Compra"


@receiver(post_delete, sender=Compras_Detalle)
def detalle_compra_borrar(sender,instance, **kwargs):
    id_producto = instance.producto.id
    id_compra = instance.compra.id

    compra_encabezado = Compras_Encabezado.objects.filter(pk=id_compra).first()
    if compra_encabezado:
        sub_total = Compras_Detalle.objects.filter(compra=id_compra).aggregate(Sum('sub_total'))
        descuento = Compras_Detalle.objects.filter(compra=id_compra).aggregate(Sum('descuento'))
        compra_encabezado.sub_total=sub_total['sub_total__sum']
        compra_encabezado.descuento=descuento['descuento__sum']
        compra_encabezado.save()
    
    producto=Producto.objects.filter(pk=id_producto).first()
    if producto:
        cantidad = int(producto.existencia) - int(instance.cantidad)
        producto.existencia = cantidad
        producto.save()


@receiver(post_save, sender=Compras_Detalle)
def detalle_compra_guardar(sender,instance,**kwargs):
    id_producto = instance.producto.id
    fecha_compra=instance.compra.fecha_compra

    producto=Producto.objects.filter(pk=id_producto).first()
    if producto:
        cantidad = int(producto.existencia) + int(instance.cantidad)
        producto.existencia = cantidad
        producto.ultima_compra=fecha_compra
        producto.save()




    
