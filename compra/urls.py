from django.urls import path
from .views import Proveedores_Vista,Proveedor_Agregar, Proveedor_Editar , Proveedor_Inactivar, \
                   Compra_Vista,Compras,Compra_Detalle_Eliminar

from .reporte import reporte_compras, imprimir_compra 

urlpatterns = [
    #proveedores
    path('proveedores/', Proveedores_Vista.as_view(),name='proveedores'),
    path('proveedores/agregar', Proveedor_Agregar.as_view(),name='proveedor_agregar'),
    path('proveedores/editar/<int:pk>', Proveedor_Editar.as_view(),name='proveedor_editar'),
    path('proveedores/inactivar/<int:id>', Proveedor_Inactivar,name='proveedor_inactivar'),
    #compras
    path('compras/',Compra_Vista.as_view(),name='compras'),
    path('compras/nuevas',Compras,name='compras_nuevas'),
    path('compras/editar/<int:compra_id>',Compras,name='compras_editar'),
    path('compras/<int:compra_id>/eliminar/<int:pk>',Compra_Detalle_Eliminar.as_view(),name='compras_eliminar'),

    #reporte
    path('compras/imprimir',reporte_compras, name='imprimir_todas'),
    path('compras/<int:compra_id>/imprimir', imprimir_compra,name="imprimir"),
]

