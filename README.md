# Sistema Facturacion e Inventario

Sistema de invetario y facturacion sencilla que posee las siguientes informaciones.

1. Categoria.
2. Sub Categorias.
3. Marcas.
4. Productos.
5. Unidad de Medidas.
6. Proveedor.
7. Cliente
8. Compra.
9. Factura.

# Tecnologia Utilizada

- Python 3.7.
- DJango 4.2.
- Postgress.

# Intalacion en Local

- Commentar el codigo de Produccion y descomentar el codigo Desarrollo en el aplicativo
- Instalar virtualenv (si aún no lo tienes instalado):

```sh
    pip install virtualenv
```

- Crear un entorno virtual:

```sh
    virtualenv entorno_virtual
```

- Activar el entorno virtual:

```sh
    entorno_virtual\Scripts\activate
```

- Intalar dependencias del archivo requirements:

```sh
    pip install -r requirements.txt
```

- Para poder crear la base de dato debes de correr las migraciones (si aún no lo tienes instalado):

```sh
    python manage.py migrate
```

- Crear Usuario Maestro o super usuario en DJango

```sh
    phyton manage.py createsuperuser
```

- correr el aplicativo abrir el navegador con la direccion IP brindada.

```sh
    phyton manage.py runserver
```
